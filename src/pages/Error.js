// ACTIVITY s53

// import { Link } from "react-router-dom";

// export default function ErrorPage() {
//   return (
//     <div>
//       <h1>Page Not Found</h1>
//       <p>
//         Go back to the <Link to="/">Homepage</Link>
//       </p>
//     </div>
//   );
// }

//========================

//====== DOING THIS BECAUSE OF BANNER REFACTOR

import Banner from "../components/Banner";

export default function Error() {
  const data = {
    title: "404 - Not found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back home",
  };

  return <Banner data={data} />;
}
