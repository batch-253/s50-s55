// //Activity s50
// import { Button, Row, Card } from "react-bootstrap";

// export default function CourseCard() {
//   return (
//     <Row className="p-3">
//       <Card className="courseCard">
//         <Card.Body>
//           <Card.Title>
//             <h2>Sample Course</h2>
//           </Card.Title>
//           <Card.Text>
//             Description:
//             <br />
//             This is a sample course offering.
//             <br />
//             Price:
//             <br />
//             PhP 40,000
//           </Card.Text>
//           <Button variant="primary">Enroll</Button>
//         </Card.Body>
//       </Card>
//     </Row>
//   );
// }

// Use the state hook for this component to be able to store its state
// States are used to keep track of information related to individual components
// Syntax
// const [getter, setter] = useState(initialGetterValue);
// import { useState, useEffect } from "react";
// import PropTypes from "prop-types";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

// Checks to see if the data was successfully passed

// Example
// export default function CourseCard( props )
// {props.courseProps.name}

// ================================================================ ACTIVITY s51 ================================================================

// Deconstruct the course properties into their own variables
export default function CourseCard({ course }) {
  // console.log(props);
  // Every component receives information in a form of an object
  // console.log(typeof props);

  // Deconstruct the course properties into their own variables
  const { _id, name, description, price } = course;

  // ACTIVITY s50
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
  // console.log(useState(0));
  // function enroll() {
  //   if (seats > 0) {
  //     setCount(count + 1);
  //     setSeats(seats - 1);
  //     console.log(`Enrolled: ${count + 1}`);
  //     console.log(`Seats available: ${seats - 1}`);
  //   } else {
  //     alert("No more seats available.");
  //   }
  // }

  // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
  // This is run automatically both after initial render and for every DOM update
  // Checking for the availability for enrollment of a course is better suited here
  // [seats] is an OPTIONAL parameter
  // React will re-run this effect ONLY if any of the values contained in this array has changed from the last render / update

  // ACTIVITY s50
  // useEffect(() => {
  //   if (seats === 0) {
  //     setSeats(false);
  //   }
  // }, [seats]);
  //==== END s50

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>

        {/* <Card.Text>Seats : {seats} </Card.Text>
        <Card.Text>Enrollees : {count} </Card.Text> */}

        {/* <Button variant="primary" onClick={enroll}>
          Enroll
        </Button> */}
        <Link className="btn btn-primary" to={`/courses/${_id}`}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
}

// =================================================================== END OF ACTIVITY s51 ======================================================

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next

// CourseCard.PropTypes = {
//   // The "shape" method is used to check if a prop object conforms to a specific "shape"
//   course: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired,
//   }),
// };
