// Bootstrap grid system components(row, col) OLD
// import { Button, Row, Col, Container } from "react-bootstrap";

// export default function Banner() {
//   return (
//     <Row>
//       <Col className="p-5">
//         <h1>Zuitt Coding Bootcamp</h1>
//         <p>Opportunities for everyone, everywhere</p>
//         <Button variant="primary">Enroll Now!</Button>
//       </Col>
//     </Row>
//   );
// }

//=======================================

// ==== DOING THIS DO REFACTOR THE HOME PAGE
// Bootstrap grid system components (row, col)
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({ data }) {
  console.log(data);

  const { title, content, destination, label } = data;

  return (
    <Row>
      <Col>
        <h1>{title}</h1>
        <p>{content}</p>
        <Link to={destination}>{label}</Link>
      </Col>
    </Row>
  );
}
